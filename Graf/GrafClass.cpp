#include "pch.h"
#include <iostream>
#include "GrafClass.h"
#include <queue>

using namespace std;

graf::graf() {};

graf::graf(int a)		//����������� �����
	{
		v = a;		//���������� ������
		sv = 1;
		arr = new int*[v];  //������� ���������
		for (int i = 0; i < v; i++)
		{
			arr[i] = new int[v];
		}
		status = new int[v];		//������ �������� ������ (��������/�� ��������/�����������)
		for (int i = 0; i < v; i++)
		{
			status[i] = 1;		//���������� � ������ ������� ������ = 1
		}
	}

void graf::addroot(int a)		//�������������� ��������� �������
	{
		root = a;

	}

void graf::showgraf()		//����� ������� ������� ��������� �� �����
	{
		cout << "\n";
		for (int i = 0; i < v; i++)
		{
			for (int j = 0; j < v; j++)
			{
				cout << arr[i][j] << " ";
			}
			cout << "\n";
		}
	}

void graf::nullgraf()		//�������� ������� ���������
	{
		for (int i = 0; i < v; i++)
		{
			for (int j = 0; j < v; j++)
			{
				arr[i][j] = 0;
			}
		}
	}

void graf::edges(int a, int b)		//��������� ������� ����� � ������� ���������
	{
		arr[a - 1][b - 1] = 1;
		arr[b - 1][a - 1] = 1;		//� ����������������� ����� ������� ��������� ����������� ������������ ������� ���������
	}

void graf::deep(int node)		//����� ����� � ������� � ����� ����� �� �����
	{
		int count = 0;
		for (int i = 0; i < v; i++)
		{
			if (arr[node][i] == 1)
			{
				cout << node + 1 << "-->" << i + 1 << " ";
				this->deep(i);
			}
			else count++;
			if (count == v)
			{
				ways++;
				cout << "\n";
			}
		}
	}

void graf::breadth(int root)		//����� ����� � ������
{
	if (status[root] != 3)		//���� ���� ��� �� �������, �� ������������� ������� � ������� ��������� ������� ������� � ��� ������
	{
		for (int i = 0; i < v; i++)
		{
			if (arr[root][i] == 1 && status[i] == 1)		//���� ������� ������ ��������������� � ��� �� �����������, �� ��������� � � �������
			{
				q.push(i);
				status[i] = 2;		//������ ������ ������� �� 2 (� � �����������, �� ��� �� ��������
			}
		}
		status[root] = 3;
		if (!q.empty())
		{
			int a = 0;
			a = q.front();
			q.pop();
			breadth(a);
		}

	}
	for (int i = 0; i < v; i++)		//���� ����� ������ ����� ����� �������� ������������ �������, �� �������� ����� ������������ ���
	{
		if (status[i] == 1)
		{
			sv++;
			breadth(i);
		}
	}
}



void graf::CycloDyff(int e)			//����� ������� ��������������� ���������
{
	int m = e - v + sv;
	cout << "\nCyclomatic complexity = " << m;
}





void graf::svshow()
{
	cout << "Number of connectivity components = " << sv;
}
