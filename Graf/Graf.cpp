﻿// Graf.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "GrafClass.h"

using namespace std;

int main()
{
	int num = 0;
	cout << "\nEnter the number of vertices: ";
	cin >> num;		//Вводим количество вершин
	graf G(num);			//Передаём в конструктор класса graf количество вершин для создания матрицы смежности
	int root = 0;
	cout << "\nEnter the number of the starting node: ";
	cin >> root;		//Вводим номер начального узла
	root--;
	G.addroot(root);
	G.nullgraf();
	G.showgraf();
	cout << "\nEnter the number of edges: ";
	int edges = 0;
	cin >> edges;			//Вводим количество рёбер
	for (int i = 0; i < edges; i++)
	{
		int a = 0;
		int b = 0;
		cout << "\nEnter the first node of edge: ";
		cin >> a;		//Начальная вершина ребра
		cout << "\nEnter the second node of edge: ";
		cin >> b;		//Конечная вершина ребра
		G.edges(a, b);	//Передаём обе вершина ребра методу edges класса graf
		cout << i + 1 << " edge added";
	}
	G.showgraf();		//Метод показывает матрицу смежности графа
	G.breadth(root);
	G.svshow();
	G.CycloDyff(edges);		//Метод подсчитывает цикломатическую сложность графа
}